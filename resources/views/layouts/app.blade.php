<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    @if (WP_ENV == CHECKDEVENV)
      @if (get_field('google_tag_manager_noscript', 'option'))
        {!! get_field('google_tag_manager_noscript', 'option') !!}
      @endif
      @if (get_field('facebook_pixel_body', 'option'))
        {!! get_field('facebook_pixel_body', 'option') !!}
      @endif
    @endif
    @if (get_option('custom_after_body', 'option')) {
      {!! get_field('custom_after_body', 'option') !!}
    @endif
    @php do_action('get_header') @endphp
    @include('partials.header')
    <div class="wrap container" role="document">
      <div class="content">
        <main class="main">
          @yield('content')
        </main>
        @if (App\display_sidebar())
          <aside class="sidebar">
            @include('partials.sidebar')
          </aside>
        @endif
      </div>
    </div>
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp

    @if(get_option('footer_script', 'option'))
      {!! get_field('footer_script', 'option')  !!}
    @endif
  </body>
</html>
