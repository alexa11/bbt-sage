<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  @if (WP_ENV == CHECKDEVENV)
    @if (get_option('google_tag_manager', 'option'))
      {!! get_field('google_tag_manager', 'option') !!}
    @endif
    @if (get_option('facebook_pixel_header', 'option'))
      {!! get_field('facebook_pixel_header', 'option') !!}
    @endif
    @if (get_option('google_analytics', 'option'))
      {!! get_field('google_analytics', 'option') !!}
    @endif

    <meta name="robots" content="noindex">
    @if (get_option('seo_thumb', 'option'))
      <meta name="theme-color" content="{!! get_field('seo_thumb', 'option') !!}"/>
    @endif
    @if (get_option('color_thumb', 'option'))
      <meta name="thumbnail" content="{!! get_field('color_thumb', 'option') !!}"/>
    @endif
  @endif


  @if (get_option('seo_favicon', 'option'))
    <link rel="shortcut icon" href="{!! get_field('seo_favicon', 'option') !!}" type="image/x-icon">
    <link rel="apple-touch-icon" href="{!! get_field('seo_favicon', 'option') !!}">
    <meta name="msapplication-square310x310logo" content="{!! get_field('seo_favicon', 'option') !!}">
  @endif

  @if (get_option('custom_before_header', 'option'))
  {!! get_field('custom_before_header', 'option') !!}
  @endif

  <!-- OG protocol -->
  @if (get_option('og_title', 'option'))
  <meta property="og:title" content="{!! get_field('og_title', 'option') !!}"/>
  @endif
  @if(get_option('og_description', 'option'))
    <meta property="og:description" content="{!! get_field('og_description', 'option') !!}"/>
  @endif
  @if(get_option('og:image', 'option'))
    <meta property="og:image" content="{!! get_field('og_img', 'option') !!}"/>
  @endif
  @if(get_option('og_others', 'option'))
    {!! get_field('og_others', 'option') !!}
  @endif

  @php wp_head() @endphp
</head>
