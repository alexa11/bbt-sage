// import external dependencies
import 'jquery';
import 'bootstrap/dist/js/bootstrap';

// Import Slick
import 'slick-carousel/slick/slick.min';

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import home from './routes/home';

/** Populate Router instance with DOM routes */
const routes = new Router({
    // Home page
    home,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
