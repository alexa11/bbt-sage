<?php

define('CHECKDEVENV', 'development');

/*Required BBT Functions*/

//Disable update wordpress nag
add_action('admin_head', 'hide_update_notice_to_all_but_admin_users', 1);
function hide_update_notice_to_all_but_admin_users()
{
    if (!current_user_can('update_core')) {
        remove_action('admin_notices', 'update_nag', 3);
    }
}

//Remove wordpress version number
add_filter('the_generator', 'wpb_remove_version');
function wpb_remove_version()
{
    return '';
}

//Hide login errors in wordpress
add_filter('login_errors', 'no_wordpress_errors');
function no_wordpress_errors()
{
    return 'Something is wrong!';
}

// Remove welcome to wordpress from dashboard
remove_action('welcome_panel', 'wp_welcome_panel');

// Disable XML-RPC in Wordpress
add_filter('xmlrpc_enabled', '__return_false');

// Add custom logo to wp login
add_action('login_enqueue_scripts', 'wpb_login_logo');
function wpb_login_logo()
{
    global $post;
    $admin_logo =  get_theme_file_path()."/resources/assets/images/admin-logo.png";
    if(file_exists($admin_logo)) { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_template_directory_uri() ?>/assets/images/admin-logo.png);
            background-repeat: no-repeat;
            background-size: 300px 100px;
            height: 100px;
            padding-bottom: 10px;
            width: 300px;
        }

        #wp-submit {
            background: black;
            border: none;
            box-shadow: 0px 0px 0px transparent;
            text-shadow: none;
        }

        #login .message, #login .success, #login #login_error {
            border-left: 4px solid black;
        }
    </style>
    <?php
    }
}

/*Custom Functions*/
/**
 * Include the TGM_Plugin_Activation class.
 */
require_once(get_theme_file_path().'/plugins/class-tgm-plugin-activation.php');

add_action('tgmpa_register', 'bbt_register_required_plugins');

/**
 * Register the required plugins for this theme.
 */
function bbt_register_required_plugins()
{
    /*
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(
        array(
            'name' => 'Advanced Custom Fields PRO',
            // The plugin name.
            'slug' => 'advanced-custom-fields-pro',
            // The plugin slug (typically the folder name).
            'source' => $_SERVER['DOCUMENT_ROOT'] . '/app/plugins/advanced-custom-fields-pro.zip',
            // The plugin source.
            'required' => true,
            // If false, the plugin is only 'recommended' instead of required.
            'version' => '',
            // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
            'force_activation' => false,
            // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
            'force_deactivation' => false,
            // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
            'external_url' => '',
            // If set, overrides default API URL and points to an external URL.
            'is_callable' => '',
            // If set, this callable will be be checked for availability to determine if a plugin is active.
        ),

    );

    /*
     * Array of configuration settings. Amend each line as needed.
     *
     */
    $config = array(
        'id' => 'bbt-tgmpa',
        // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',
        // Default absolute path to bundled plugins.
        'menu' => 'bbt-tgmpa-install-plugins',
        // Menu slug.
        'parent_slug' => 'themes.php',
        // Parent menu slug.
        'capability' => 'edit_theme_options',
        // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
        'has_notices' => true,
        // Show admin notices or not.
        'dismissable' => true,
        // If false, a user cannot dismiss the nag message.
        'dismiss_msg' => '',
        // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,
        // Automatically activate plugins after installation or not.
        'message' => '',
        // Message to output right before the plugins table.
    );

    tgmpa($plugins, $config);
}


//Add theme options page
if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title' => 'Theme General Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Theme Scripts',
        'menu_title' => 'Scripts',
        'parent_slug' => 'theme-general-settings',
    ));
}


add_action('acf/init', 'my_acf_add_local_field_groups');

function my_acf_add_local_field_groups()
{
    //add script fields
    acf_add_local_field_group(array(
        'key' => 'group_theme_settings_scripts',
        'title' => 'SCRIPTS -- Should be wrapped in script or noscript tags or comments only',
        'fields' => array(
            array(
                'key' => 'field_google_optimize',
                'label' => 'Google Optimize',
                'name' => 'google_optimize',
                'type' => 'textarea',
                'placeholder' => '<script></script>'
            ),
            array(
                'key' => 'field_google_tag_manager',
                'label' => 'Google Tag Manager',
                'name' => 'google_tag_manager',
                'type' => 'textarea',
                'placeholder' => '<script></script>',
                'instructions' => 'include the script needed before the '
            ),
            array(
                'key' => 'field_google_tag_manager_noscript',
                'label' => 'Google Tag Manager (noscript)',
                'name' => 'google_tag_manager_noscript',
                'type' => 'textarea',
                'placeholder' => '<noscript></noscript>'
            ),
            array(
                'key' => 'field_facebook_pixel_header',
                'label' => 'Facebook Pixel (Header)',
                'name' => 'facebook_pixel_header',
                'type' => 'textarea',
                'placeholder' => '<script></script>'
            ),
            array(
                'key' => 'field_facebook_pixel_body',
                'label' => 'Facebook Pixel (Body)',
                'name' => 'facebook_pixel_body',
                'type' => 'textarea',
                'placeholder' => '<script></script>'
            ),
            array(
                'key' => 'field_google_analytics',
                'label' => 'Google Analytics',
                'name' => 'google_analytics',
                'type' => 'textarea',
            ),
            array(
                'key' => 'field_custom_before_header',
                'label' => 'Custom Script - Before < /head> Tag',
                'name' => 'custom_before_header',
                'type' => 'textarea',
                'placeholder' => '<script></script>'
            ),
            array(
                'key' => 'field_custom_after_body',
                'label' => 'Custom Script - After Body Tag < body > Tag ',
                'name' => 'custom_after_body',
                'type' => 'textarea',
                'placeholder' => '<script></script>'
            ),
            array(
                'key' => 'field_footer_script',
                'label' => 'Custom Script - Footer Script ',
                'name' => 'footer_script',
                'type' => 'textarea',
                'placeholder' => '<script></script>'
            )

        ),
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-scripts',
                ),
            ),
        ),
    ));

    acf_add_local_field_group(array(
        'key' => 'group_theme_settings_header',
        'title' => 'Header Group',
        'fields' => array(
            array(
                'key' => 'field_header_logo',
                'label' => 'Header Logo',
                'name' => 'header_logo',
                'type' => 'image',
            )
        ),
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'theme-general-settings',
                ),
            ),
        ),
    ));

    acf_add_local_field_group(array(
        'key' => 'group_theme_settings_footer',
        'title' => 'Footer Group',
        'fields' => array(
            array(
                'key' => 'field_footer_logo',
                'label' => 'Footer Logo',
                'name' => 'footer_logo',
                'type' => 'image',
            ),
            array(
                'key' => 'field_copyright',
                'label' => 'Copyright',
                'name' => 'copyright',
                'type' => 'text',
            )
        ),
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'theme-general-settings',
                ),
            ),
        ),
    ));

    acf_add_local_field_group(array(
        'key' => 'group_theme_settings_SEO',
        'title' => 'SEO Requirements',
        'fields' => array(
            array(
                'key' => 'field_favicon',
                'label' => 'Favicon',
                'name' => 'seo_favicon',
                'type' => 'image',
                'return_format' => 'url',
                'instructions' => 'It must point to a non-transparent 192px (or 180px) square PNG'
            ),
            array(
                'key' => 'field_seo_thumb',
                'label' => 'Thumbnails meta',
                'name' => 'seo_thumb',
                'type' => 'image',
                'return_format' => 'url'
            ),
            array(
                'key' => 'field_color_thumb',
                'label' => 'Theme Color meta',
                'name' => 'color_thumb',
                'type' => 'text',
                'instructions' => 'Should be in Hex format #FFFFFF'
            )

        ),
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'theme-general-settings',
                ),
            ),
        ),
    ));

    //Create field for OG
    //add script fields
    acf_add_local_field_group(array(
        'key' => 'theme_setting_post_group',
        'title' => 'Open Graph protocol settings',
        'fields' => array(
            array(
                'key' => 'field_og_title',
                'label' => 'og:title',
                'name' => 'og_title',
                'type' => 'text',
                'placeholder' => 'og:title',
                'instructions' => 'The title of your object as it should appear within the graph, e.g., "The Rock".'
            ),
            array(
                'key' => 'field_og_desc',
                'label' => 'og:description',
                'name' => 'og_description',
                'type' => 'textarea',
                'placeholder' => 'og:title',
                'instructions' => 'A one to two sentence description of your object.'
            ),
            array(
                'key' => 'field_og_img',
                'label' => 'og:img',
                'name' => 'og_img',
                'type' => 'image',
                'return_format' => 'url',
                'placeholder' => 'og:img',
                'instructions' => 'An image URL which should represent your object within the graph.'
            ),
             array(
                'key' => 'field_og_others',
                'label' => 'og:other',
                'name' => 'og_others',
                'type' => 'textarea',
                'instructions' => 'Include HTML meta tag '
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                )
            ),
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'page',
                )
            ),
        ),
    ));
}